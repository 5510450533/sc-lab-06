package test2;

public abstract class Animal {

	private String name;

	public Animal(String name){
		this.name = name;
	}
	public abstract String jump();

	public String toString(){
		return this.name;
	}

}
